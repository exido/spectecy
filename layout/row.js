import styled, { css } from 'styled-components';
import { constructMargin } from '../helpers/margins';

const Row = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-left: 0;
  margin-right: 0;
  ${props => props.margin && css`
    margin: ${constructMargin(props.margin)};
  `}
  & > [class~="Col-"] {
    padding-left: ${props => props.gapLess && 0 };
    padding-right: ${props => props.gapLess && 0 };
  }
  
`;

export default Row;
