import styled, {css} from 'styled-components';
import { variables } from '../base';
import { constructSize, getWidth, getMargins, constructMargin } from '../helpers';


const colSet = ['xs', 'sm', 'md', 'lg', 'xl'];

const Col = styled.div`
  margin-left: ${props => getMargins(props, 'left')};
  margin-right: ${props => getMargins(props, 'right')};
  flex-wrap: ${props => props.oneLine ? 'nowrap' : 'wrap'};
  overflow-x: ${props => props.oneLine && 'auto'};
  flex: ${props => props.col ? 'none' : props.colAuto ? '0 0 auto' : 1};
  max-width: ${props => props.col ? '100%' : props.colAuto ? 'none' : '100%'};
  padding-left: ${constructSize(variables.layoutSpacing, 'rem')};
  padding-right: ${constructSize(variables.layoutSpacing, 'rem')};
  width: ${props => props.col && getWidth(props.col)};
  transition: all 0.3s ease-in-out;
  
  ${props => props.textCenter && css`
    text-align: center;
  `}
  ${props => props.margin && css`
    margin: ${constructMargin(props.margin)};
  `}
  
  @media (max-width : ${variables.sizexl}px) {
    flex: ${props => props.xl && 'none'};
    width: ${props => props.xl && getWidth(props.xl)};
  }
  @media (max-width : ${variables.sizelg}px) {
    flex: ${props => props.lg && 'none'};
    width: ${props => props.lg && getWidth(props.lg)}
  }
  @media (max-width : ${variables.sizemd}px) {
    flex: ${props => props.md && 'none'};
    width: ${props => props.md && getWidth(props.md)}
  }
  @media (max-width : ${variables.sizesm}px) {
    flex: ${props => props.sm && 'none'};
    width: ${props => props.sm && getWidth(props.sm)}
  }
  @media (max-width : ${variables.sizexs}px) {
    flex: ${props => props.xs && 'none'};
    width: ${props => props.xs && getWidth(props.xs)}
  }
`;

export default Col;