import styled from 'styled-components';
import { variables } from '../base';
import { constructSize } from '../helpers';

const Container = styled.div`
  margin-left: auto;
  margin-right: auto;
  padding-left: ${constructSize(variables.layoutSpacingLg,'rem')};
  padding-right: ${constructSize(variables.layoutSpacingLg,'rem')};
  width: 100%;
  max-width: ${props => props.grid ? constructSize((variables[ `size${props.grid}`]),'px') : 'inherit'};
  display: ${props => props.show ? 'none !important' : 'block'};
  
`;

export default Container;