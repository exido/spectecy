import { css } from 'styled-components';
import variables from './_variables';

const typography = css`
.serif{
  font-family: ${variables.baseSerifFamily}
}
h1,
h2,
h3,
h4,
h5,
h6 {
  color: inherit;
  font-weight: 400;
  line-height: 1.2;
  margin-bottom: .5em;
  margin-top: 0;
}
h1{
  font-size: 2.5rem;
}
h2 {
  font-size: 2rem;
}
h3{
  font-size: 1.75rem;
}
h4 {
  font-size: 1.5rem;
}
h5 {
  font-size: 1.25rem;
}
h6 {
  font-size: 1rem;
}

p {
  margin: 0 0 ${variables.lineHeight};
}

a,
ins,
u {
  text-decoration-skip: ink edges;
}

ul,
ol {
  margin: ${variables.unit4} 0 ${variables.unit4} ${variables.unit4};
  padding: 0;

  ul,
  ol {
    margin: ${variables.unit4} 0 ${variables.unit4} ${variables.unit4};
  }

  li {
    margin-top: ${variables.unit2};
  }
}

ul {
  list-style: disc inside;

  ul {
    list-style-type: circle;
  }
}

ol {
  list-style: decimal inside;

  ol {
    list-style-type: lower-alpha;
  }
}

dl {
  dt {
    font-weight: bold;
  }
  dd {
    margin: ${variables.unit2} 0 ${variables.unit4} 0;
  }
}
`;

export default typography;