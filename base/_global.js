import { createGlobalStyle, css } from 'styled-components';
import variables from './_variables';
import normalize from './_normalize';
import typography from './typography';

const GlobalStyle = createGlobalStyle`
@font-face {
  font-family: 'ExivarSans';
  src: url('/fonts/Exivar Sans-Regular.woff2') format('woff2'), /* Modern Browsers */
       url('/fonts/Exivar Sans-Regular.otf') format('opentype'); /* Safari, Android, iOS */
           font-style: normal;
  font-weight: 400;
  text-rendering: optimizeLegibility;
  font-display: fallback;
}
@font-face {
  font-family: 'ExivarSans';
  src: url('/fonts/Exivar Sans-Medium.woff2') format('woff2'), /* Modern Browsers */
       url('/fonts/Exivar Sans-Medium.otf') format('opentype'); /* Safari, Android, iOS */
           font-style: normal;
  font-weight: 500;
  text-rendering: optimizeLegibility;
  font-display: fallback;
}
@font-face {
  font-family: 'ExivarSans';
  src: url('/fonts/Exivar Sans-Bold.woff2') format('woff2'), /* Modern Browsers */
       url('/fonts/Exivar Sans-Bold.otf') format('opentype'); /* Safari, Android, iOS */
           font-style: normal;
  font-weight: bold;
  text-rendering: optimizeLegibility;
  font-display: fallback;
}
@font-face {
  font-family: 'ExivarSerif';
    src: url('/fonts/Exivar Serif-Regular.woff2') format('woff2'), /* Modern Browsers */
         url('/fonts/Exivar Serif-Regular.otf') format('opentype'); /* Safari, Android, iOS */
             font-style: normal;
    font-weight: 400;
    text-rendering: optimizeLegibility;
}
@font-face {
    font-family: 'ExivarSerifSemBd';
    src: url('/fonts/Exivar Serif-SemiBold.woff2') format('woff2'), /* Modern Browsers */
         url('/fonts/Exivar Serif-SemiBold.otf') format('opentype'); /* Safari, Android, iOS */
             font-style: normal;
    font-weight: 500;
    text-rendering: optimizeLegibility;
}
@font-face {
    font-family: 'ExivarSerif';
    src: url('/fonts/Exivar Serif-Bold.woff2') format('woff2'), /* Modern Browsers */
         url('/fonts/Exivar Serif-Bold.otf') format('opentype'); /* Safari, Android, iOS */
             font-style: normal;
    font-weight: bold;
    text-rendering: optimizeLegibility;
}
*,
*::before,
*::after {
  box-sizing: inherit;
}
html,body {
  padding: 0;
  margin: 0;
}
html {
  box-sizing: border-box;
  font-size: ${variables.htmlFontSize};
  line-height: ${variables.lineHeight};
  -webkit-tap-highlight-color: transparent;
  font-family: ${variables.baseFontFamily};
  font-weight: 400;
  width: 100%;
  height: 100vh;
  position: relative;
}
body {
  font-family: ${variables.baseFontFamily};
  font-size: ${variables.htmlFontSize};
  overflow-x: hidden;
  text-rendering: optimizeLegibility;
}
${props => !props.theme && css`
  @media (prefers-color-scheme: dark) {
      body {
          background-color: #111;
          color: white;
      }
  }
  @media (prefers-color-scheme: light) {
      body {
          background-color: white;
          color: #111;
      }
  }
`}
${props => props.theme && props.theme === 'dark' && css`
  body {
        background-color: ${variables.primaryColor};
        color: white;
    }
    a {
      color: white;
      outline: none;
      text-decoration: none;

      &:focus,
      &:hover,
      &:active,
      &.active {
        color: white;
        text-decoration: underline;
      }
      &:visited {
        color: white;
      }
    }
`}
${props => props.theme && props.theme === 'light' && css`
  body {
      background-color: white;
      color: ${variables.primaryColor};
  }
`}
${normalize}
${typography}
`;

export default GlobalStyle;