import constructSize from '../helpers/constructSize';
// default fixed sizings
const baseFontSize = 16;
const primaryColor = '#111';

// base variables
const baseFontFamily = '"ExivarSans", -apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto';
const baseSerifFamily = '"ExivarSerif", Times New Roman';
const htmlFontSize = constructSize(baseFontSize, 'px');
const lineHeight = 1.3;

// unit sizes
const unito = 0.05;
const unith = 0.1;
const unit1 = 0.2;
const unit2 = 0.4;
const unit3 = 0.6;
const unit4 = 0.8;
const unit5 = 1;
const unit6 = 1.2;
const unit7 = 1.4;
const unit8 = 1.6;
const unit9 = 1.8;
const unit10 = 2;
const unit12 = 2.4;
const unit16 = 3.2;

// Sizes
const layoutSpacing = unit2;
const layoutSpacingSm = unit1;
const layoutSpacingLg = unit4;
const borderRadius = unith;
const borderWidth = unito;
const borderWidthLg = unith;
const controlSize = unit9;
const controlSizeSm = unit7;
const controlSizeLg = unit10;
const controlPaddingX = unit2;
const controlPaddingXSm = unit2 * 0.75;
const controlPaddingXLg = unit2 * 1.5;
const controlPaddingY = (controlSize - lineHeight) / 2 - borderWidth;
const controlPaddingYSm = (controlSizeSm - lineHeight) / 2 - borderWidth;
const controlPaddingYLg = (controlSizeLg - lineHeight) / 2 - borderWidth;
const controlIconSize = constructSize(0.8, 'rem');
const gridSpacing = (layoutSpacing / (layoutSpacing * 0 + 1)) * baseFontSize;

// Responsive breakpoints
const sizexs = 480;
const sizesm = 768;
const sizemd = 1024;
const sizelg = 1200;
const sizexl = 1440;
const sizexxl = 1920;

const variables = {
  primaryColor,
  baseFontSize,
  baseFontFamily,
  baseSerifFamily,
  htmlFontSize,
  lineHeight,
  unito,
  unith,
  unit1,
  unit2,
  unit3,
  unit4,
  unit5,
  unit6,
  unit7,
  unit8,
  unit9,
  unit10,
  unit12,
  unit16,
  layoutSpacing,
  layoutSpacingSm,
  layoutSpacingLg,
  borderRadius,
  borderWidth,
  borderWidthLg,
  controlSize,
  controlSizeSm,
  controlSizeLg,
  controlPaddingX,
  controlPaddingXSm,
  controlPaddingXLg,
  controlPaddingY,
  controlPaddingYSm,
  controlPaddingYLg,
  controlIconSize,
  gridSpacing,
  sizexs,
  sizesm,
  sizemd,
  sizelg,
  sizexl,
  sizexxl,
};

export default variables;
