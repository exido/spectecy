const getWidth = size => size === 'auto' ? 'auto' : `${parseFloat(100 / (12 / Number(size))).toFixed(2)}%`;

export default getWidth;
