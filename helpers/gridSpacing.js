import {variables} from '../base';

const gridSpacing = () => (variables.layoutSpacing / (variables.layoutSpacing * 0 + 1)) * variables.baseFontSize;

export default gridSpacing;
