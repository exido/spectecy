import constructSize from './constructSize';

export const getMargins = (type, direction) => {
  switch (true) {
  case type.mxAuto:
    return 'auto';
  case type.mlAuto && direction === 'left':
    return 'auto';
  case type.mrAuto && direction === 'right':
    return 'auto';
  default:
    return null;
  }
};
export const constructMargin = (size, type = 'rem') => `${constructSize(size[0], type)} ${constructSize(size[1], type)} ${constructSize(size[2], type)} ${constructSize(size[3], type)}`;
