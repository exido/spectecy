# README #

Add Spectecy as a submodule to your local repository.

if you're using Next.js, you can create a path for it to be accessible in the application.
```
{
  "compilerOptions": {
    "baseUrl": ".",
    "paths": {
      "@spectecy/*": ["spectecy/*"],
    }
  }
}
```

If you're using Exivar Fonts, copy them in Public folder so that they can be accessible for NEXT.js